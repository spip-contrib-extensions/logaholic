<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_descriptif' => 'Ici vous pouvez indiquer votre identifiant Logaholic.',
	'cfg_titre' => 'Logaholic',

	// E
	'explication_id_logaholic' => 'Si "_" ou vide, supprime la fonctionnalit&eacute; (vide reviendra au d&eacute;faut). ne reneseigner que le numéro',
	'explication_lwa_server' => 'Saisisser l\'adresse du site sur le quel vous pouvez consulter les statistiques.',

	// L
	'label_id_logaholic' => 'Votre identifiant Logaholic du type "LWA_p1"',
	'label_lwa_server' => 'Url du site de statistique',
);
?>